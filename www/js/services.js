angular.module('starter.services', [])

  .factory('Account', function() {
    var user = {
      id: 1,
      account: 'test',
      password: 'test',
      name: 'Tester'
    };
    return {
      aboutMe: function() {
        return user;
      },
      toParam: function() {
        return 'account=' + user.account + '&password=' + user.password;
      }
    };
  })
  .factory('Utils', function () {
    return {
      secondsToDate: function (seconds) {
        var d = new Date();
        d.setTime(seconds);
        return d.toLocaleString();
      },
      findIndex: function (list, id) {
        for (var i in list) {
          if (list[i].id === id) {
            return i;
          }
        }
      },
      replaceUrl: function (photo) {
        photo.url = photo.url.replace(/^\/images/, 'https://jw.zjut.party/images');
        // Test
        //photo.url = photo.url.replace(/^\/images/, 'http://127.0.0.1/images');
      },
      getAPI: function (path) {
        return 'https://jw.zjut.party/api/v1/' + path;
        // Test
        //return 'http://127.0.0.1:8080/v1/' + path;
      }
    };
  })

  .factory('Stream', function ($http, Account, Utils) {
    var url = Utils.getAPI('posts/');
    var posts = [];
    return {
      getAll: function (callback) {
        $http.get(url).then(function (resp) {
          if (resp.data.status === 200) {
            posts = resp.data.response;
          }
          callback(posts);
        });
      },
      postComment: function(postId, text, callback) {
        console.log('ppp', text);
        $http({
          method: 'POST',
          url: url + postId + '/comments?' + Account.toParam(),
          data: {'content': text}
        }).then(function (resp) {
          console.log('add comment done', text);
          if (resp.data.status === 201) {
            callback();
          } else {
            console.log(resp);
          }
        });
      },
      remove: function (postId, callback) {
        $http.delete(url + postId + '?' + Account.toParam())
        .then(function (resp) {
          if (resp.data.status === 204) {
            var idx = Utils.findIndex(posts, postId);
            posts.splice(idx, 1);
          }
          callback(posts);
        });
      },
      update: function (post, callback) {
        $http.post(url + post.id + '?' + Account.toParam(), post)
        .then(function (resp) {
          if (resp.data.status === 202 && resp.data.response) {
            var idx = Utils.findIndex(posts, post.id);
            if (idx >= 0) {
              post = resp.data.response;
              Utils.replaceUrl(post.photo);
              if (post.photo.userId === Account.aboutMe().id) {
                post.mine = true;
              }
              posts[idx] = post;
            }
          }
          callback(posts);
        });
      }
    };
  })

  .factory('Photos', function ($http, Account, Utils) {
    var url = Utils.getAPI('photos/');
    var photos = [];
    var getPhotos = function (callback) {
      $http.get(url + '?' + Account.toParam())
        .then(function (resp) {
          photos = resp.data.response;
          callback(photos);
        });
    };
    return {
      getAll: function (callback, refresh) {
        if (refresh) {
          getPhotos(callback);
        } else {
          callback(photos);
        }
      },
      get: function (photoId, callback) {
        var getPhoto = function () {
          var idx = Utils.findIndex(photos, photoId);
          if (idx >= 0) {
            callback(photos[idx]);
          } else {
            // TODO
          }
        };
        if (photos.length == 0) {
          getPhotos(getPhoto);
        } else {
          getPhoto();
        }
      },
      remove: function (photoId, callback) {
        $http.delete(url + photoId + '?' + Account.toParam())
          .then(function (resp) {
            if (resp.data.status == 204) {
              var idx = Utils.findIndex(photos, photoId);
              photos.splice(idx, 1);
            }
            callback(photos);
          });
      },
      sharePhoto: function (post, callback) {
        var post_url = Utils.getAPI('posts');
        $http.post(post_url + '?' + Account.toParam(), post)
          .then(function (resp) {
            if (resp.data.status === 201) {
              callback();
            }
          });
      },
      update: function (photo, callback) {
        $http.post(url + photo.id + '?' + Account.toParam(), photo)
          .then(function (resp) {
            if (resp.data.status === 202 && resp.data.response) {
              var idx = Utils.findIndex(photos, photo.id);
              if (idx >= 0) {
                photo = resp.data.response;
                Utils.replaceUrl(photo);
                photos[idx] = photo;
              }
            }
            callback(photo);
          });
      }
    };
  })

  .factory('Camera', function ($http, $q, Account, Utils) {
    var url = Utils.getAPI('photos/');
    return {
      uploadImage: function (imageUri, title, callback) {
        var uploadUrl = url +'upload?' + Account.toParam();
        var options = new FileUploadOptions();
        options.fileKey = 'file';
        options.mimeType = 'image/jpeg';
        options.fileName = title + '.jpg';
        var onError = function (resp) {
          console.log('Upload error', JSON.stringify(resp));
        };
        var ft = new FileTransfer();
        ft.upload(imageUri, uploadUrl, callback, onError, options);
      },
      upload: function (photo, callback) {
        $http.post(url + '?' + Account.toParam(), photo)
          .then(function (resp) {
            if (resp.data.status === 201 && resp.data.response) {
              callback(resp.data.response);
            }
          });
      },
      getPicture: function (options) {
        var q = $q.defer();
        navigator.camera.getPicture(function(result) {
          q.resolve(result);
        }, function(err) {
          q.reject(err);
        }, options);
        return q.promise;
      }
    };
  })

  .factory('Profile', function ($http, Account, Utils) {
    var url = Utils.getAPI('me/');
    var bookmarks = 'unknown';
    return {
      getBookmarks: function (refresh, callback) {
        if (refresh || bookmarks === 'unknown') {
          $http.get(url + 'bookmarks?' + Account.toParam())
            .then(function (resp) {
              if (resp.data.status === 200) {
                bookmarks = resp.data.response;
                callback(bookmarks);
              } else {
                // TODO
              }
            });
        } else {
          callback(bookmarks);
        }
      }
    };
  })
;
