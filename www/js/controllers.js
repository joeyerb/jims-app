angular.module('starter.controllers', [])

  .controller('HomeCtrl', function ($scope, $http, Stream, Profile, Account, Utils) {
    $scope.posts = [];

    // Posts
    $scope.getAllPosts = function () {
      console.log('get all posts');
      Stream.getAll(function (posts) {
        console.log('got all posts');
        Profile.getBookmarks(true, function (bookmarks) {
          console.log('got all bookmarks');
          $scope.posts = posts.map(function (post) {
            Utils.replaceUrl(post.photo);
            if (post.photo.userId === Account.aboutMe().id) {
              post.mine = true;
            }
            post.liked = false;
            for (var i in bookmarks) {
              if (bookmarks[i].post.id == post.id) {
                post.liked = true;
              }
            }
            //console.log('done post: ' + JSON.stringify(post));
            return post;
          });
        });
      });
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.getAllPosts();

  })

  .controller('PostsCtrl', function ($scope, $http, $ionicPopover, $ionicModal, $timeout, $ionicPopup,
                                    $ionicActionSheet, Stream, Profile, Utils, Account) {

    $scope.secondsToDate = Utils.secondsToDate;
    $scope.aboutMe = Account.aboutMe;

    var base_url = Utils.getAPI('');
    var url = base_url + 'posts/';

    var myId = Account.aboutMe().id;

    // Posts
    var updatePost = function (postId) {
      $http.get(url + postId).then(function (resp) {
        if (resp.data.status === 200) {
          var idx = Utils.findIndex($scope.posts, postId);
          var post = resp.data.response;
          if (idx >= 0) {
            Utils.replaceUrl(post.photo);
            if (post.photo.userId === myId) {
              post.mine = true;
            }
            post.liked = false;
            Profile.getBookmarks(true, function (bookmarks) {
              for (var i in bookmarks) {
                if (bookmarks[i].post.id === post.id) {
                  post.liked = true;
                }
              }
            });
            post.showComments = true;
            $scope.posts[idx] = post;
          } else {
            $scope.posts.push(post);
          }
        }
      });
    };

    $scope.updatePost = function (post) {
      //var post = $scope.currentPost;
      if (post.newContent && post.content != post.newContent) {
        post.content = post.newContent;
        Stream.update({id: post.id, content: post.newContent}, function (posts) {
          $scope.posts = posts;
          console.log('update done');
        });
      }
    };

    // Popover menu
    //document.body.classList.remove('platform-android');
    //document.body.classList.add('platform-ios');
    $ionicPopover.fromTemplateUrl('templates/post-popover.html', {
      scope: $scope
    }).then(function(popover) {
      $scope.popover = popover;
    });
    $scope.showPopover = function ($event, post) {
      console.log('show pop', post.id);
      $scope.currentPost = post;
      $scope.popover.show($event);
    };

    $scope.rmPost = function () {
      console.log('remove post 1', $scope.currentPost.id);
      Stream.remove($scope.currentPost.id, function (posts) {
        $scope.posts = posts;
        console.log('remove done');
      });
      $scope.popover.hide();
    };

    $scope.editPost = function () {
      $scope.currentPost.editable = true;
      $scope.popover.hide();
    };

    // Tag
    $ionicModal.fromTemplateUrl('templates/addtag-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });
    $scope.openTagModal = function() {
      $scope.modal.show();
    };
    $scope.closeTagModal = function() {
      $scope.popover.hide();
      $scope.modal.hide();
    };
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

    $scope.addTag = function () {
      var post = $scope.currentPost;
      var tag = {
        name: post.newTag
      };
      $http.post(base_url + 'photos/' + post.photo.id
          + '/tags?' + Account.toParam(), tag)
        .then(function (resp) {
          if (resp.data.status === 201) {
            updatePost(post.id);
          }
        });
      $scope.popover.hide();
      $scope.closeTagModal();
    };

    $scope.showTagPopup = function(post, tag) {
      var rmTag = function (post, tag) {
        $http.delete(base_url + 'photos/' + post.photo.id + '/tags/'
            + tag.name + '?' + Account.toParam())
          .then(function (resp) {
            if (resp.data.status === 204) {
              updatePost(post.id);
            }
          });
      };

      var confirmPopup = $ionicPopup.confirm({
        title: 'Remove tag',
        template: 'Are you sure you want to remove tag: <b>' + tag.name + '</b>?'
      });

      confirmPopup.then(function(res) {
        if(res) {
          console.log('remove');
          rmTag(post, tag);
        } else {
          console.log('cancel');
        }
      });
    };

    // TODO set private

    // Like
    var unlike = function (post) {
      var bkId = null;
      Profile.getBookmarks(false, function (bookmarks) {
        for (var i in bookmarks) {
          if (bookmarks[i].post.id === post.id) {
            bkId = bookmarks[i].id;
          }
        }
        if (bkId) {
          $http.delete(base_url + 'me/bookmarks/' + bkId + '?' + Account.toParam())
            .then(function (resp) {
              if (resp.data.status == 204) {
                updatePost(post.id);
              }
              console.log('removed bookmark', bkId);
            });
        }
      });
    };
    var addLike = function (postId, rating) {
      console.log('add bookmark');
      rating = rating || 10;
      $http.post(base_url + 'me/bookmarks?' + Account.toParam(), {
        postId: postId,
        rating: rating
      }).then(function(resp) {
        if (resp.data.status == 201) {
          updatePost(postId);
        }
      });
    };
    $scope.toggleLike = function (post) {
      return post.liked ? unlike(post) : addLike(post.id);
    };

    // Comments
    $scope.toggleComments = function (post) {
      post.showComments = post.showComments ? false : true;
    };
    $scope.addComment = function (postId, text) {
      if (text) {
        Stream.postComment(postId, text, function() {
          updatePost(postId);
        });
      }
    };
    $scope.rmComment = function(postId, commentId) {
      $http.delete(url + postId + '/comments/' + commentId + '?' + Account.toParam())
        .then(function (resp) {
          if (resp.data.status == 204) {
            updatePost(postId);
          }
        })
    };

    // Share // TODO
    $scope.showShareSheet = function() {

      var hideSheet = $ionicActionSheet.show({
        buttons: [
          { text: '<b>Tweet</b>' },
          { text: 'Instagram' },
          { text: '...' }
        ],
        titleText: 'Share to',
        cancelText: 'Cancel',
        cancel: function() {
          // TODO
        },
        buttonClicked: function(index) {
          return true;
        }
      });

      $timeout(function() {
        hideSheet();
      }, 4000);

    };

  })

  .controller('PhotosCtrl', function ($scope, $http, Photos, Utils) {
    $scope.secondsToDate = Utils.secondsToDate;

    var getPhotos = function (refresh) {
      Photos.getAll(function (photos) {
        $scope.photos = photos.map(function (photo) {
          Utils.replaceUrl(photo);
          return photo;
        });
      }, refresh);
    };

    $scope.rmPhoto = function (photoId) {
      Photos.remove(photoId, function (photos) {
        $scope.photos = photos;
      });
    };

    $scope.$on('$ionicView.enter', function () {
      getPhotos(false);
    });

    getPhotos(true);

  })

  .controller('PhotoDetailCtrl', function ($scope, $stateParams, $ionicModal, $ionicHistory, Photos, Utils) {
    $scope.secondsToDate = Utils.secondsToDate;

    Photos.get(parseInt($stateParams.photoId), function (photo) {
      $scope.photo = photo;
    });

    // Delete
    $scope.rmPhoto = function (photoId) {
      Photos.remove(photoId, function () {
        $ionicHistory.goBack();
      });
    };

    // Edit
    $scope.editInfo = function (photo) {
      if (photo.editable) {
        photo.editable = false;
        if (photo.caption !== photo.newCaption) {
          photo.caption = photo.newCaption;
          Photos.update(photo, function (photo) {
            $scope.photo = photo;
          });
        }
      } else {
        photo.editable = true;
      }
    };

    // Share
    $scope.newPost = {
      postType: 'photo',
      photoId: $scope.photo.id,
      content: "",
      publicity: 'public'
    };
    $scope.share = function () {
      if ($scope.newPost.content) {
        Photos.sharePhoto($scope.newPost, function () {
          console.log('post done');
          $scope.modal.hide();
          $scope.newPost.content = "";
        });
      } else {
        console.log('empty post');
      }
    };

    // Share modal
    $ionicModal.fromTemplateUrl('share-modal', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      console.log('new modal');
      $scope.modal = modal;
    });
    $scope.showShareModal = function () {
      console.log('show modal');
      $scope.modal.show();
    };
    $scope.hideShareModal = function () {
      $scope.modal.hide();
    };
    $scope.$on('$destroy', function() {
      $scope.modal.remove();
    });

  })

  .controller('CameraCtrl', function ($scope, Camera, Photos) {

    $scope.reset = function () {
      $scope.images = [];
      $scope.photoInfo = {};

      $scope.newPost = {
        postType: 'photo',
        content: "",
        publicity: 'public'
      };
    };

    $scope.reset();

    $scope.uploadPhoto = function () {
      var photo = $scope.photoInfo;
      console.log('uploading', photo);
      if (photo.title && $scope.images.length) {
        var image = $scope.images[0];
        // Upload image
        Camera.uploadImage(image.uri, $scope.photoInfo.title, function (resp) {
          image.uploaded = true;
          console.log('up done', JSON.stringify(resp));
          // Then info
          Camera.upload(photo, function (photo) {
            console.log('upload info done');
            console.log(JSON.stringify(photo));
            $scope.photoInfo = {};
            $scope.images = [];
            // Then post
            $scope.newPost.photoId = photo.id;
            if ($scope.newPost.content && photo.id) {
              Photos.sharePhoto($scope.newPost, function () {
                console.log('post done');
                $scope.newPost.content = "";
                $scope.newPost.photoId = 0;
              });
            } else {
              console.log('empty post');
            }
          });
        });
      }
    };

    $scope.addPhoto = function() {
      Camera.getPicture().then(function (imageUri) {
        var imgCache = {uri: imageUri, uploaded: false};
        $scope.images.push(imgCache);
      }, function (err) {
        console.log(err);
      }, {
        quality: 50,
        targetWidth: 800,
        targetHeight: 800,
        saveToPhotoAlbum: true,
        mediaType: 2
      });
    };

  })

  .controller('ProfileCtrl', function ($scope) {
  })

  .controller('BookmarksCtrl', function ($scope, Profile, Utils, Account) {
    $scope.posts = [];
    $scope.secondsToDate = Utils.secondsToDate;
    $scope.aboutMe = Account.aboutMe;

    $scope.getBookmarks = function () {
      Profile.getBookmarks(true, function (bookmarks) {
        $scope.posts = bookmarks.map(function (bookmark) {
          Utils.replaceUrl(bookmark.post.photo);
          if (bookmark.post.photo.userId === Account.aboutMe().id) {
            bookmark.post.mine = true;
          }
          bookmark.post.liked = true;
          return bookmark.post;
        });
      });
      $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.getBookmarks();
  })

  .controller('MyPostsCtrl', function ($scope, Stream, Profile, Utils, Account) {
    $scope.posts = [];
    $scope.secondsToDate = Utils.secondsToDate;
    $scope.aboutMe = Account.aboutMe;

    // TODO
    $scope.getMyPosts = function () {
      var myId = Account.aboutMe().id;
      Stream.getAll(function (posts) {
        Profile.getBookmarks(true, function (bookmarks) {
          $scope.posts = posts.filter(function (post) {
            if (post.photo.userId === myId) {
              Utils.replaceUrl(post.photo);
              if (post.photo.userId === myId) {
                post.mine = true;
              }
              post.liked = false;
              for (var i in bookmarks) {
                if (bookmarks[i].post.id == post.id) {
                  post.liked = true;
                }
              }
              return true;
            }
            return false;
          });
        });
      });
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    };

    $scope.getMyPosts();
  })

  .controller('SettingsCtrl', function ($scope) {
    $scope.settings = {
      enableFriends: true
    };
  })
;
